package cardealership;

public class Truck extends Car {
    private int height;
    private double cargoCapacity;

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getCargoCapacity() {
        return this.cargoCapacity;
    }

    public void setCargoCapacity(double cargoCapacity) {
        this.cargoCapacity = cargoCapacity;
    }

    public Truck(String make, String model, int year, double mileage, Condition condition, int height, double cargoCapacity) {
        super(make, model, year, mileage, condition, "sportsCar");
        this.height = height;
        this.cargoCapacity = cargoCapacity;
    }


    @Override
    public void displayInfo() {
        System.out.print("This is a "+getYear()+" "+getModel()+" "+getMake()+" truck. It has "+getMileage()+" of mileage and has a "+getCondition()+" condition.\n It is "+getHeight()+"m tall and has a "+getCargoCapacity()+"kg cargoCapacity.");
    }
    
}
