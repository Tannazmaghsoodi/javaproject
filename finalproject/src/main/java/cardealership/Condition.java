package cardealership;

public enum Condition {
    BAD {
        public String toString(){
            return "BAD";
        }
    },
    AVERAGE {
        public String toString(){
            return "AVERAGE";
        }
    },
    GOOD {
        public String toString(){
            return "GOOD";
        }
    },
    VERYGOOD {
        public String toString(){
            return "VERY GOOD";
        }
    },
    BEST{
        public String toString(){
            return "BEST";
        }
    }
}
