package cardealership;

import java.util.ArrayList;

public class Location {
    private Inventory inventory;
    private int locationID;
    private String city;

    public int getLocationID() {
        return this.locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Inventory getInventory() {
        return this.inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Location (String city, int locationID){
        this.city = city;
        this.locationID = locationID;
        inventory = new Inventory(new ArrayList<Car>());
    }

    public void transferCar(){

    }
}
