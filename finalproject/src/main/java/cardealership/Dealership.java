package cardealership;

import java.util.ArrayList;
import java.util.List;

public class Dealership {
    private List<Location> locations = new ArrayList<Location>();

    public Dealership(List<Location> locations){
        this.locations = locations;
    }

    public List<Location> getLocations() {
        return this.locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public void sortCarPerLocation(List<Car> carList){
        for(Car car: carList){
            for(Location location: this.locations){
                if(car.getLocationID() == location.getLocationID()){


                    location.getInventory().addCar(car);
                    System.out.println("added");
                }
            }
        }
    }

    public List<Car> getAllCarsAcrossLocations(){
        List<Car> carlist = new  ArrayList<Car>();      
        for (Location location: locations){
            for (Car car: location.getInventory().getCarList()){
                carlist.add(car);
            }
        }
        return carlist;
    }
}
