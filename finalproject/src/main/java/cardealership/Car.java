package cardealership;

public abstract class Car {
    private String make;
    private String model;
    private int year;
    private double mileage;
    private Condition condition;
    private String Type;

   
    public Car(String make, String model, int year, double mileage, Condition condition, String type){
        this.make = make;
        this.model = model;
        this.year = year;
        this.mileage = mileage;
        this.condition = condition;
        this.Type = type;
    }

    public String getMake() {
        return this.make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int Year) {
        this.year = Year;
    }

    public double getMileage() {
        return this.mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    public Condition getCondition() {
        return this.condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public String getType() {
        return this.Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public abstract void displayInfo();

    // public boolean isAvailableForTestDrive(){
    
    // };
    
}
