package cardealership;

public class SportsCar extends Car{
    private double zeroToHundred;

    public double getZeroToHundred() {
        return this.zeroToHundred;
    }

    public void setZeroToHundred(int zeroToHundred) {
        this.zeroToHundred = zeroToHundred;
    }

    public SportsCar(String make, String model, int year, double mileage, Condition condition, double zeroToHundred) {
        super(make, model, year, mileage, condition, "sportsCar");
        this.zeroToHundred = zeroToHundred;
    }


    @Override
    public void displayInfo() {
        System.out.print("This is a "+getYear()+" "+getModel()+" "+getMake()+" sportsCar. It has "+getMileage()+" of mileage and has a "+getCondition()+" condition.\n It goes from 0-100 in a whopping "+getZeroToHundred()+" seconds!");
    }
    
}
