package cardealership;

public class User {
    private String userName;
    private String password;
    private String role;

    public User(String userName, String passWord, String role){
        this.userName = userName;
        this.password = passWord;
        this.role = role;
    }
    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
