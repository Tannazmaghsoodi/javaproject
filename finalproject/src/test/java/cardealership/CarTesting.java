package cardealership;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * testing class for the car abstract class and its subtypes
 */
public class CarTesting {
    
    public void testSettersAndGettersAndConstructor(){
        Car sportsCar = new SportsCar("sport1", "sport2", 1, 2.0, Condition.GOOD, 4.5);
        Car truck = new Truck("truck1", "truck2", 1, 2.0, Condition.GOOD, 3, 3000);

        assertEquals(sportsCar, truck);
    }
}
